package com.ecommerce.microcommerce.web.controller;

import com.ecommerce.microcommerce.configuration.SwaggerConfig;
import com.ecommerce.microcommerce.dao.ProductDao;
import com.ecommerce.microcommerce.model.Product;
import com.ecommerce.microcommerce.web.exceptions.ProduitGratuitException;
import com.ecommerce.microcommerce.web.exceptions.ProduitIntrouvableException;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Api(tags = { SwaggerConfig.TAG_PRODUCTCONTROLLER })
@RestController
public class ProductController {

    @Autowired
    private ProductDao productDao;

    @ApiOperation(value = "Récupération d'une liste de produits trié par nom.")
    @GetMapping(value = "/Produits")
    public MappingJacksonValue listeProduits() {

        List<Product> produits = productDao.getAllByOrderByNom();

        SimpleBeanPropertyFilter monFiltre = SimpleBeanPropertyFilter.serializeAllExcept("prixAchat", "id");

        FilterProvider listDeNosFiltres = new SimpleFilterProvider().addFilter("monFiltreDynamique", monFiltre);

        MappingJacksonValue produitsFiltres = new MappingJacksonValue(produits);

        produitsFiltres.setFilters(listDeNosFiltres);

        return produitsFiltres;
    }

    @ApiOperation(value = "Récupère un produit grâce à son ID à condition que celui-ci soit en stock!")
    @GetMapping(value = "/Produits/{id}")
    public MappingJacksonValue afficherUnProduit(@PathVariable int id) {

        SimpleBeanPropertyFilter monFiltre = SimpleBeanPropertyFilter.serializeAll();

        FilterProvider listDeNosFiltres = new SimpleFilterProvider().addFilter("monFiltreDynamique", monFiltre);

        Product product = productDao.findById(id);

        if(product==null) throw new ProduitIntrouvableException("Le produit avec l'id " + id + " est INTROUVABLE.");

        MappingJacksonValue produitFiltres = new MappingJacksonValue(product);

        produitFiltres.setFilters(listDeNosFiltres);

        return produitFiltres;

    }

    @ApiOperation(value = "Insersion d'un produit en stock.")
    @PostMapping(value = "/Produits")
    public ResponseEntity<Void> ajouterProduit(@RequestBody Product product) {

        if (product.getPrix() == 0) throw new ProduitGratuitException("Le produit avec l'id " + product.getId() + " n'a pas de prix de vente.");

        Product productAdded = productDao.save(product);

        if (productAdded == null) throw new ProduitIntrouvableException("Le produit avec l'id " + product.getId() + " est INTROUVABLE.");

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(productAdded.getId())
                .toUri();

        return ResponseEntity.created(location).build();
    }

    @GetMapping(value = "test/ProduitsGreatherThan/{prixLimit}")
    public MappingJacksonValue getProductWithPriceGreaterThan(@PathVariable int prixLimit) {
        List<Product> liste = productDao.findByPrixGreaterThan(prixLimit);

        SimpleBeanPropertyFilter monFiltre = SimpleBeanPropertyFilter.serializeAllExcept("prixAchat", "id");

        FilterProvider listDeNosFiltres = new SimpleFilterProvider().addFilter("monFiltreDynamique", monFiltre);


        MappingJacksonValue produitFiltres = new MappingJacksonValue(liste);

        produitFiltres.setFilters(listDeNosFiltres);

        return produitFiltres;
    }

    @GetMapping(value = "test/ProduitByNom/{recherche}")
    public MappingJacksonValue productByNom(@PathVariable String recherche) {
        List<Product> liste = productDao.findByNomLike("%" + recherche + "%");

        SimpleBeanPropertyFilter monFiltre = SimpleBeanPropertyFilter.serializeAllExcept("prixAchat", "id");

        FilterProvider listDeNosFiltres = new SimpleFilterProvider().addFilter("monFiltreDynamique", monFiltre);


        MappingJacksonValue produitFiltres = new MappingJacksonValue(liste);

        produitFiltres.setFilters(listDeNosFiltres);

        return produitFiltres;
    }

    @ApiOperation(value = "Suppression d'un produit en stock.")
    @DeleteMapping(value = "/Produits/{id}")
    public ResponseEntity<Void> supprimerProduit(@PathVariable int id) {

        Product p = productDao.findById(id);

        if (p == null) throw new ProduitIntrouvableException("Le produit avec l'id " + id + " est INTROUVABLE.");

        productDao.delete(p);
        return ResponseEntity.ok().build();
    }

    @ApiOperation(value = "Modification d'un produit à condition que celui-ci soit en stock!")
    @PutMapping(value = "/Produits")
    public ResponseEntity<Void> updateProduit(@RequestBody Product product) {

        Product productInStock = productDao.findById(product.getId());

        if (productInStock == null) throw new ProduitIntrouvableException("Le produit avec l'id " + product.getId() + " est INTROUVABLE.");

        Product p = productDao.save(product);

        if (p == null) throw new ProduitIntrouvableException("Le produit avec l'id " + product.getId() + " est INTROUVABLE.");

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(p.getId())
                .toUri();

        return ResponseEntity.created(location).build();

    }

    @ApiOperation(value = "Récupération des marges de vente des produits stock.")
    @GetMapping(value = "/AdminProduits")
    public ResponseEntity<HashMap<Product,Integer>> calculMargeProduit(){

        HashMap<Product,Integer> resultList = new HashMap<>();
        List<Product> productList = productDao.findAll();

        if(productList.isEmpty()) return ResponseEntity.notFound().build();

        for (Product p: productList) {
            resultList.put(p,(p.getPrix()-p.getPrixAchat()));
        }

        return new ResponseEntity<>(resultList, HttpStatus.OK);
    }

}
