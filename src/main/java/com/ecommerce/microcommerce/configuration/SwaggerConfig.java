package com.ecommerce.microcommerce.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    public static final String TAG_PRODUCTCONTROLLER = "ProductController";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.ecommerce.microcommerce.web"))
                .paths(PathSelectors.regex("/Produits.*"))
                .build()
                .apiInfo(apiInfo())
                .tags(new Tag(TAG_PRODUCTCONTROLLER, "Controlleur réalisant toutes les méthodes liées au produit"));
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("ECOMMERCE_API").version("1.0.0").build();
    }
}
